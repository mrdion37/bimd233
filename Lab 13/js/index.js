function Flight(name, destination, numSeats, numSeatsTaken, dateTimeLeaving) 
{
  this.name = name;
  this.destination = destination;
  this.numSeats = numSeats;
  this.numSeatsTaken = numSeatsTaken;
  this.checkAvailability = function() {
    return this.numSeats - this.numSeatsTaken;
  }
};


//For the data, I decided to come up with it on the top of my head, it is a lot less variables, but you still get the idea. I might add more variables later
var flight1 = new Flight("Alaska", "Hawaii", 200, 129, "Dec 12 2019 12:20:00");

var flight2 = new Flight("United", "New Orleans", 100, 19, "Oct 14 2020 15:20:00");

var flight3 = new Flight("Delta", "Seattle", 100, 9, "Sept 16 2019 18:30:00");

var flight4 = new Flight("Alaska", "Brazil", 250, 160, "Jan 01 2019 00:10:00");

var flight5 = new Flight("Delta", "Vietnam", 150, 150, "Mar 12 2021 09:54:00");

var allFlights = [flight1, flight2, flight3. flight4, flight5];

var el = document.getElementById('table_data');
el.innerHTML = allFlights[1]