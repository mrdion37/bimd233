var highTemps = [82, 75, 69, 69, 68];
var lowTemps = [55, 52, 52, 48, 51];

var highLength = highTemps.length - 1;
var lowLength = lowTemps.length - 1;
var highTempSum = 0;
var lowTempSum = 0;
var count = 0;
while (highLength >= 0 && lowLength >= 0) 
{
  highTempSum += highTemps[highLength];
  lowTempSum += lowTemps[lowLength];
  highLength = highLength - 1;
  lowLength = lowLength - 1;
  count++;
}
highTempSum /= count;
lowTempSum /= count;

function getAvg(highTempSum, lowTempSum) {
  return (highTempSum + lowTempSum)/2;
}


//Output functions
function myFunction() 
{
document.getElementById("demo").innerHTML = "Average low temerature: " + lowTempSum;
}

function myFunction1()
{
document.getElementById("demo1").innerHTML = "Average high temperature: " + highTempSum;
}

function myFunction2()
{
  document.getElementById("Avg of avgs").innerHTML = "Average of high and low temperatures: " + getAvg(highTempSum, lowTempSum);
  //document.getElementById("Avg of avgs").innerHTML = "test: " + highLength + "," + highTemps.length;
}